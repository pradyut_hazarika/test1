# split_data.py
from sklearn.model_selection import train_test_split
import pandas as pd
from get_data import read_params 
import argparse

args = argparse.ArgumentParser()
args.add_argument("--config", required=True)
parsed_args = args.parse_args()
confs = read_params(parsed_args.config) 
df = pd.read_csv(confs["base"]["project_path"] + "data_given/SAHeart.csv")
df.to_csv(confs["base"]["project_path"] + "data/raw/SAHeart.csv")