# split_data.py
from sklearn.model_selection import train_test_split
import pandas as pd
from get_data import read_params
from get_data import read_params
import argparse

args = argparse.ArgumentParser()
args.add_argument("--config", required=True)
parsed_args = args.parse_args()
confs = read_params(parsed_args.config)
df = pd.read_csv(confs["base"]["project_path"] + "data/raw/SAHeart.csv").drop(['Unnamed: 0','Unnamed: 0.1'], axis=1)
encoded_df = pd.get_dummies(df, columns=['cp', 'restecg', 'slope', 'thal'])
train, test = train_test_split(encoded_df, test_size=confs["split_data"]["test_size"], random_state=confs["base"]["random_state"])
train.to_csv(confs["base"]["project_path"] + confs["split_data"]["train_path"])
test.to_csv(confs["base"]["project_path"] + confs["split_data"]["test_path"])