# train_and_evaluate.py
import joblib
import pandas as pd
from sklearn.linear_model import LogisticRegression
import json
from sklearn.metrics import roc_auc_score, accuracy_score
from get_data import read_params
import argparse

args = argparse.ArgumentParser()
args.add_argument("--config", required=True)
parsed_args = args.parse_args()
confs = read_params(parsed_args.config)

train = pd.read_csv( confs["base"]["project_path"] + confs["split_data"]["train_path"])
x_train = train.drop(["target"],axis=1)
y_train = train["target"]

model = LogisticRegression(random_state=5)
model.fit(x_train, y_train)
joblib.dump(model, confs["base"]["project_path"] + confs["model_dir"] + "/model.joblib", compress=9)

test = pd.read_csv(confs["base"]["project_path"] + confs["split_data"]["test_path"])
x_test = test.drop(["target"],axis=1)
y_test = test["target"]
y_pred = model.predict(x_test)
report = {
    "roc_auc_score": roc_auc_score(y_test, y_pred),
    "test_score": accuracy_score(y_test, y_pred),
    "train_score": accuracy_score(y_train, model.predict(x_train)),
}

scores = pd.DataFrame()
scores["actual"] = y_test
scores["pred"] = y_pred

scores.to_csv(confs["base"]["project_path"] + "plots/cm.csv", index=False)
json.dump( report,open( confs["base"]["project_path"] + "report/scores.json", "w") )