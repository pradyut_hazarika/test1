# load_data.py
import pandas as pd
import yaml
import argparse

def read_params(config_path):
    conf = None
    with open(config_path, "r") as stream:
        try:
            conf = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return conf

def get_data(confs): 
    df = pd.read_csv(
                "../data/heart.csv",
            )
    df.to_csv( confs["base"]["project_path"] + "data_given/SAHeart.csv")

args = argparse.ArgumentParser()
args.add_argument("--config", required=True)
parsed_args = args.parse_args()
confs = read_params(parsed_args.config) 
get_data(confs)